# Marukochan
<!-- CI Badges go here. -->
Makurochan is a tool that allows you to input what materials you have and tells you what items you can make with those materials. It's named after [a villager](https://animalcrossing.fandom.com/wiki/Bettina) in the game who aspires to be a chef.     
<br><br>

## Git Flow Overview
The different branches are used as follows:  

| Branch | Purpose |
| ------: | ------ |
| **master** | Currently live on the website. Merges are squashed from *dev*. |
| **dev** | Upcoming, completed features. Merges are squashed from *feature/X* | 
| **feature/x** | Feature development. Only ever live on my sandbox. | 
<br>

## _Want to Fork?_
Please feel free to use my code as a reference for your own work. With that said, at this time I am requesting that you do not _copy_ this code. Thank you for your understanding and happy hacking!
<br><br>

## _Got Questions?_ 
I'm happy to answer them! Just [send me an email](mailto:mp.middleton@outlook.com)!
